resource "aws_s3_bucket" "this" {
  bucket_prefix = "${var.bucket_prefix}-"
  force_destroy = !var.protection
}

resource "aws_s3_bucket_public_access_block" "this" {
  bucket                  = aws_s3_bucket.this.id
  block_public_acls       = !var.public
  block_public_policy     = !var.public
  ignore_public_acls      = !var.public
  restrict_public_buckets = !var.public
}
