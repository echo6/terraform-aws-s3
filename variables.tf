variable "bucket_prefix" {
  type        = string
  description = "https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket#bucket_prefix"
}

variable "public" {
  default = false
}

variable "protection" {
  default = false
}
