module "this" {
  source        = "../"
  bucket_prefix = var.git
}

terraform {
  backend "s3" {}
}

variable "git" {}
