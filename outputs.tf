output "id" {
  value       = aws_s3_bucket.this.id
  description = "https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket#id"
}
